#!/usr/bin/python3
#
# wut-img-ck.py
#
# Validate images.

import os
import glob
from PIL import Image

# All of download...
for waterfall in glob.glob('/srv/satnogs/download/*/waterfall*.png'):
    print(waterfall)
    v_image = Image.open(waterfall)
    v_image.verify()

# Individual training dirs
for waterfall in glob.glob('/srv/satnogs/data/train/good/waterfall*.png'):
    print(waterfall)
    v_image = Image.open(waterfall)
    v_image.verify()

for waterfall in glob.glob('/srv/satnogs/data/train/bad/waterfall*.png'):
    print(waterfall)
    v_image = Image.open(waterfall)
    v_image.verify()

for waterfall in glob.glob('/srv/satnogs/data/val/good/waterfall*.png'):
    print(waterfall)
    v_image = Image.open(waterfall)
    v_image.verify()

for waterfall in glob.glob('/srv/satnogs/data/val/bad/waterfall*.png'):
    print(waterfall)
    v_image = Image.open(waterfall)
    v_image.verify()

