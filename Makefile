# Makefile

prefix = /usr/local
bindir = $(prefix)/bin

all:
	$(MAKE) -C src

clean:
	rm -fr bin/

install:
	@cp -vp bin/*  $(bindir)/

uninstall:
	@rm -vf \
	$(bindir)/wut \
	$(bindir)/wut-aria-active \
	$(bindir)/wut-aria-add \
	$(bindir)/wut-aria-daemon \
	$(bindir)/wut-aria-info \
	$(bindir)/wut-aria-methods \
	$(bindir)/wut-aria-shutdown \
	$(bindir)/wut-aria-stat \
	$(bindir)/wut-aria-stopped \
	$(bindir)/wut-aria-waiting \
	$(bindir)/wut-audio-archive \
	$(bindir)/wut-audio-sha1 \
	$(bindir)/wut-compare \
	$(bindir)/wut-compare-all \
	$(bindir)/wut-compare-tx \
	$(bindir)/wut-compare-txmode \
	$(bindir)/wut-compare-txmode-csv \
	$(bindir)/wut-dl-sort \
	$(bindir)/wut-dl-sort-tx \
	$(bindir)/wut-dl-sort-txmode \
	$(bindir)/wut-dl-sort-txmode-all \
	$(bindir)/wut-files \
	$(bindir)/wut-files-data \
	$(bindir)/wut-files-data-all \
	$(bindir)/wut-ia-sha1 \
	$(bindir)/wut-ia-torrents \
	$(bindir)/wut-img-ck.py \
	$(bindir)/wut-ml \
	$(bindir)/wut-ml-auto \
	$(bindir)/wut-ml-load \
	$(bindir)/wut-ml-save \
	$(bindir)/wut-obs \
	$(bindir)/wut-ogg2wav \
	$(bindir)/wut-review-staging \
	$(bindir)/wut-rm-random \
	$(bindir)/wut-tf \
	$(bindir)/wut-tf.py \
	$(bindir)/wut-water \
	$(bindir)/wut-water-range \
	$(bindir)/wut-worker \
	$(bindir)/wut-worker-mas \
	$(bindir)/wut-worker-mas.py \
	$(bindir)/wut-worker.py \

