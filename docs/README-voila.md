# Voila

Voila is a way to turn Jupyter notebooks into web applications.

# Install
Start with basic Debian Buster install.

```
# set up partitions
# XXX deps...
sudo apt update
sudo apt install apache2 python3-certbot-apache python3-pip sshfs npm nodejs
certbot
systemctl restart apache2
adduser wut
sudo su - wut
pip3 install --user --upgrade pip
# make sure new `pip3` at `~/.local/bin/pip3` is in front in `$PATH`.
echo 'PATH=~/.local/bin:$PATH' >> ~/.bashrc
```

logout #log back in as user wut
sudo su - wut
# Install Python packages for Voila
pip3 install --user --upgrade -r requirements-voila.txt
# Enable Jupyter extensions
jupyter nbextension enable --py widgetsnbextension
#jupyter labextension install @jupyter-widgets/jupyterlab-manager
#jupyter serverextension enable --py jupyterlab --user
```

* Set up hosts file, network, etc.

* Set up apache proxy

```
# Cruft to start voila:
cd /srv/satnogs/satnogs-wut/notebooks/

voila                                                                   \
        --ExecutePreprocessor.timeout=600                               \
        --no-browser                                                    \
        --port=8867                                                     \
        --autoreload=True                                               \
        --Voila.ip=localhost                                            \
        --VoilaConfiguration.enable_nbextensions=False                  \
        --theme=dark                                                    \
        wut-web.ipynb                                                   \
        1>>~/log/voila.log 2>>~/log/voila.err &
```


# wut?
Site:

* https://spacecruft.org/spacecruft/satnogs-wut/

